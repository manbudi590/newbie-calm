import 'package:Good_Job/ui/login_page.dart';
import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';

class Splashskrin extends StatefulWidget {
  @override
  _SplashskrinState createState() => _SplashskrinState();
}

class _SplashskrinState extends State<Splashskrin> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 5,
      navigateAfterSeconds: LoginPage(),
      image: Image.network("https://previews.123rf.com/images/petr73/petr731501/petr73150100045/35305512-red-oval-stamp-with-the-words-good-job-vector.jpg"),
      photoSize: 100.0,
      loaderColor: Colors.blue[100],
      loadingText: Text("Harap Menunggu"),
    );
  }
}