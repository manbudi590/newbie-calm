import 'package:flutter/material.dart';
import 'package:Good_Job/komponen/splash.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Good Job',
      theme: new ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: new Splashskrin(), debugShowCheckedModeBanner: false,
    );
  }
}