import 'package:cloud_firestore/cloud_firestore.dart';

class User {
  var id;
  var nama;
  var email;
  var password;

  static User fromDocument(DocumentSnapshot doca) {
    User taskP = User();
    taskP.id = doca.documentID;
    taskP.nama = doca.data["name"];
    taskP.email = doca.data["email"];
    taskP.password = doca.data["password"];
    return taskP;
  }
}

