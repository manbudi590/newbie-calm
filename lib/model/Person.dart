import 'package:cloud_firestore/cloud_firestore.dart';

class Person {
  String nama;
  String alamat;
  String keterangan;
  String photoUrl = "";

  toMap() {
    return {
      'nama': nama,
      'alamat': alamat,
      'keterangan': keterangan,
      'photo': photoUrl,
    };
  }

  static Person fromDocument(DocumentSnapshot doc) {
    var p = Person();
    p.nama = doc['nama'];
    p.alamat = doc['alamat'];
    p.keterangan = doc['keterangan'];
    p.photoUrl = doc['photo'];

    return p;
  }
}
