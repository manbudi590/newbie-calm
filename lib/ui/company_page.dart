import 'package:Good_Job/ui/detail_company.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CompanyPage extends StatefulWidget {
  @override
  _OlahanPageState createState() => _OlahanPageState();
}

class _OlahanPageState extends State<CompanyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.orange,
        centerTitle: true,
        title: Text("Company", style: TextStyle(
          color: Colors.white,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w600,
          fontSize: 25.0 ,
        ),),
      ),

      body:  Container(
        child: GridView.count(
        crossAxisCount: 2,
        padding: EdgeInsets.all(16.0),
        childAspectRatio: 0.9,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 10.0,
        children: <Widget>[
          GestureDetector(
          child: myGridItems("Bank Indonesia", "https://indonesia.go.id/assets/img/kabinet/1546404912_BI.jpg", 0xFFEF9A9A, 0xFFE57373),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailCompany()));
          },
          ),
          GestureDetector(
          child: myGridItems("Bank BRI", "https://image.cermati.com/f_auto,q_70/x8ghrweipfy5a75ycqem", 0xFFEF9A9A, 0xFFE57373),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailCompany()));
          },
          ),       
        
        ],
      ),
          ),
          
    );
}
  Widget myGridItems(String gridName, String gridimage, int color, int color1){
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(24.0),
      gradient: new LinearGradient(
        colors: [
          Color(color),
          Color(color1),
        ],
        begin: Alignment.centerLeft,
        end: new Alignment(1.0,1.0), 
      ),
    ),

    child: Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.3,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              image: DecorationImage(
                image: new NetworkImage(gridimage),
                fit: BoxFit.fill,
              )
            ),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
             Container(
               child:  Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 10.0,),
                  Container(child: Text("Jasa", style: TextStyle(color: Colors.white,fontSize: 16.0),),),
                  SizedBox(width: 10.0,),
                  Container(child: Icon(Icons.motorcycle),),
                  SizedBox(width: 10.0,),
                  Container(child: Text("Antar", style: TextStyle(color: Colors.white,fontSize: 16.0),),),
                  SizedBox(height: 10.0,),
                                  ],),),
                  Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(gridName,style: TextStyle(color:Colors.white,fontSize: 20.0, fontWeight: FontWeight.bold)),
                  ),
          ],
        )
      ],
    ),
  );

  }
}