import 'package:Good_Job/ui/detail_freelancer.dart';
import 'package:Good_Job/ui/tambah_data.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'detail_company.dart';
import 'package:Good_Job/model/Person.dart';

class FreelancerPage extends StatefulWidget {
  @override
  _FreelancerPageState createState() => _FreelancerPageState();
}

class _FreelancerPageState extends State<FreelancerPage> {

  var listPeople = List<Person>();
  Person data;
  
  @override
  void initState() {
    super.initState();
    _loadFromFirebase();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.pink,
        centerTitle: true,
        title: Text("Freelancer", style: TextStyle(
          color: Colors.white,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w600,
          fontSize: 25.0 ,
        ),),
      ),

      body: Container(
        child: GridView.count(
        crossAxisCount: 2,
        padding: EdgeInsets.all(15.0),
        childAspectRatio: 0.9,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 10.0,
        children: <Widget>[
          GestureDetector(
          
          child: myGridItems("Kotlin Developer", "https://d540vms5r2s2d.cloudfront.net/mad/uploads/mad_blog_5c054535668f11543849269.png", 0xFFF48FB1, 0xFFF06292),
          onTap: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailFreelancer()));
          },
          ), 
          GestureDetector(
          child: myGridItems("Flutter Developer", "https://blog.codemagic.io/uploads/CM_Android-dev-Flutter.9c02e273ebcd875d17b80037b2147c68cc0f5055dd3f1b9d663ebedec3d66ba7.png", 0xFFF48FB1, 0xFFF06292),
          onTap: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailFreelancer()));
          },
          ),        
        
        ],
          ),
      ),
          floatingActionButton: FloatingActionButton(
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>TambahData()));
          },
          child: Icon(Icons.add),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(16.0))),
        ),  
          
    );
}
  Widget myGridItems(String gridName, String gridimage, int color, int color1){
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(24.0),
      gradient: new LinearGradient(
        colors: [
          Color(color),
          Color(color1),
        ],
        begin: Alignment.centerLeft,
        end: new Alignment(1.0,1.0), 
      ),
    ),

    child: Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.3,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              image: DecorationImage(
                image: new NetworkImage(gridimage),
                fit: BoxFit.fill,
              )
            ),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
             Container(
               child:  Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 10.0,),
                  Container(child: Text("Freelan", style: TextStyle(color: Colors.white,fontSize: 16.0),),),
                  SizedBox(width: 10.0,),
                  Container(child: Icon(Icons.motorcycle),),
                  SizedBox(width: 10.0,),
                  Container(child: Text("Job", style: TextStyle(color: Colors.white,fontSize: 16.0),),),
                  SizedBox(height: 10.0,),
                                  ],),),
                  Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(gridName,style: TextStyle(color:Colors.white,fontSize: 20.0, fontWeight: FontWeight.bold)),
                  ),
          ],
        )
      ],
    ),
  );

  }

  void _loadFromFirebase() {
    Firestore.instance
    .collection("listData")
    .snapshots()
    .listen((value) {
      List<Person> persons = value.documents.map((doc) => Person.fromDocument(doc)).toList();
      setState(() {
        listPeople = persons;
        print(listPeople[0].nama);
        print(listPeople[0].photoUrl);
      });
    });
  }
}

