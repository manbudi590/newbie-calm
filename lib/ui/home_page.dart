import 'package:Good_Job/ui/company_page.dart';
import 'package:Good_Job/ui/freelancer_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/widgets.dart';
import 'freelancer_page.dart';
import 'company_page.dart';


class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override

  Widget build(BuildContext context) {
    return Scaffold(
body: NestedScrollView(
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: 250.0,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text("GOOD JOB",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25.0,
                        fontFamily: 'Poppins',
                        fontWeight: FontWeight.bold,
                      )),
                  background: Image.network(
                    "https://images.fastcompany.net/image/upload/w_1280,f_auto,q_auto,fl_lossy/wp-cms/uploads/2018/10/p-1-this-artificial-intelligence-wont-take-your-job-it-will-help-you-do-it-better.jpg",
                    fit: BoxFit.cover,
                  )),
            ),
          ];
        },
      body:  Container(
        padding: EdgeInsets.only(top: 30.0),
          //   decoration: BoxDecoration(
          //     gradient: LinearGradient(
          //     begin: Alignment.topRight,
          //     end: Alignment.bottomLeft,
          //      stops: [0.1, 0.5, 0.7, 0.9],
          // colors: [
          //   // Colors are easy thanks to Flutter's Colors class.
          //   Colors.blue[700],
          //   Colors.blue[600],
          //   Colors.blue[400],
          //   Colors.blue[300],
          // ],
          //     )
          //   ),
        child: GridView.count(
        crossAxisCount: 1,
        padding: EdgeInsets.all(16.0),
        childAspectRatio: 2.2,
        crossAxisSpacing: 10.0,
        mainAxisSpacing: 10.0,
        children: <Widget>[
          // GestureDetector(
          // child: myGridItems("Profil User", "https://mumbrella.com.au/wp-content/uploads/2018/05/freelancer.jpg", 0xFFEF9A9A, 0xFFE57373),
          // onTap: (){
          //   Navigator.push(context, MaterialPageRoute(builder: (context)=>Profil()));
          // },
          // ),

          GestureDetector(
          child: myGridItems("Freelancer", "https://mumbrella.com.au/wp-content/uploads/2018/05/freelancer.jpg", 0xFFEF9A9A, 0xFFE57373),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>FreelancerPage()));
          },
          ),
          GestureDetector(
          child: myGridItems("Company", "http://images.summitmedia-digital.com/esquiremagph/images/2019/03/14/FromPexelsPhilippBirmes.jpg", 0xFFF48FB1, 0xFFF06292),
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>CompanyPage()));
          },
          ),

          // GestureDetector(
          // child: myGridItems("Kegiatan Binaan", "https://www.titintenun.com/wp-content/uploads/2019/04/g.jpeg", 0xFFCE93D8, 0xFFBA68C8),
          // onTap: (){
          //   Navigator.push(context, MaterialPageRoute(builder: (context)=>KegiatanPage()));
          // },
          // ),
        
        
        ],
      ),

          ),

        ), 
    );
  }

Widget myGridItems(String gridName, String gridimage, int color, int color1){
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(24.0),
      gradient: new LinearGradient(
        colors: [
          Color(color),
          Color(color1),
        ],
        begin: Alignment.centerLeft,
        end: new Alignment(1.0,1.0), 
      ),
    ),

    child: Stack(
      children: <Widget>[
        Opacity(
          opacity: 0.3,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(24.0),
              image: DecorationImage(
                image: new NetworkImage(gridimage),
                fit: BoxFit.fill,
              )
            ),
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SizedBox(width: 10.0,),
                  Container(child: Icon(Icons.computer),),
                  SizedBox(width: 10.0,),
                  Container(child: Text("Job", style: TextStyle(color: Colors.white,fontSize: 16.0),),),
                  SizedBox(width: 10.0,),
                  Container(child: Text("Finder", style: TextStyle(color: Colors.white,fontSize: 16.0),),),
                  SizedBox(height: 10.0,),
                                  ],),),
                  Padding(
                  padding: const EdgeInsets.only(left: 10.0),
                  child: Text(gridName,style: TextStyle(color:Colors.white,fontSize: 20.0, fontWeight: FontWeight.bold)),
                  ),
          ],
        ),
      ],
    ),
  );
}
}