import 'package:flutter/material.dart';
import 'package:Good_Job/style/theme.dart' as Theme;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'freelancer_page.dart';
import 'package:Good_Job/model/Person.dart';

class TambahData extends StatefulWidget {
  @override
  _TambahDataState createState() => _TambahDataState();
}

class _TambahDataState extends State<TambahData> {
  Person person = Person();
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
    appBar: new AppBar(
        backgroundColor: Theme.Colors.loginGradientEnd,
        centerTitle: true,
        title: Text("Add Data", style: TextStyle(
          color: Colors.white,
          fontFamily: 'Poppins',
          fontWeight: FontWeight.w600,
          fontSize: 25.0 ,
        ),),
      ),
      body:  NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
        },
      child: SingleChildScrollView(
     child: Container(
      padding: EdgeInsets.only(top: 30.0),
      child: Center(
        child: Form(
          key: _formKey,
            child: Column(
              children: <Widget>[
                Stack(
                  alignment: Alignment.topCenter,
                  overflow: Overflow.visible,
                  children: <Widget>[
                    Card(
                      elevation: 2.0,
                      color: Colors.white,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Container(
                        width: 300.0,
                        height: 360.0,
                        child: Column(
                          children: <Widget>[

                            Padding(
                              padding: EdgeInsets.only(
                                  top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                              child: TextField(
                                // focusNode: myFocusNodeName,
                                // controller: signupNameController,
                                keyboardType: TextInputType.text,
                                textCapitalization: TextCapitalization.words,
                                style: TextStyle(
                                    fontFamily: "WorkSansSemiBold",
                                    fontSize: 16.0,
                                    color: Colors.black),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Icon(
                                    Icons.verified_user,
                                    color: Colors.black,
                                  ),
                                  hintText: "Nama",
                                  hintStyle: TextStyle(
                                      fontFamily: "WorkSansSemiBold", fontSize: 16.0),
                                ),
                                onChanged: (val){
                                  person.nama = val;
                                },
                              ),
                            ),
                            Container(
                              width: 250.0,
                              height: 1.0,
                              color: Colors.grey[400],
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                              child: TextField(
                                // focusNode: myFocusNodeEmail,
                                // controller: signupEmailController,
                                keyboardType: TextInputType.emailAddress,
                                style: TextStyle(
                                    fontFamily: "WorkSansSemiBold",
                                    fontSize: 16.0,
                                    color: Colors.black),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Icon(
                                    Icons.verified_user,
                                    color: Colors.black,
                                  ),
                                  hintText: "Keterangan",
                                  hintStyle: TextStyle(
                                      fontFamily: "WorkSansSemiBold", fontSize: 16.0),
                                ),
                                onChanged: (val){
                                  person.keterangan = val;
                                },
                              ),
                            ),
                            Container(
                              width: 250.0,
                              height: 1.0,
                              color: Colors.grey[400],
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                  top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                              child: TextField(
                                // focusNode: myFocusNodePassword,
                                // controller: signupPasswordController,
                                // obscureText: _obscureTextSignup,
                                style: TextStyle(
                                    fontFamily: "WorkSansSemiBold",
                                    fontSize: 16.0,
                                    color: Colors.black),
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  icon: Icon(
                                    Icons.lock,
                                    color: Colors.black,
                                  ),
                                  hintText: "Alamat",
                                  hintStyle: TextStyle(
                                      fontFamily: "WorkSansSemiBold", fontSize: 16.0),
                                  suffixIcon: GestureDetector(
                                    onTap: (){},
                                    child: Icon(
                                      Icons.panorama_fish_eye,
                                      size: 15.0,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                                onChanged: (val){
                                  person.alamat = val;
                                },
                              ),
                            ),
                            Container(
                              width: 250.0,
                              height: 1.0,
                              color: Colors.grey[400],
                            ),
                            // Padding(
                            //   padding: EdgeInsets.only(
                            //       top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                            //   child: TextField(
                            //     // controller: signupConfirmPasswordController,
                            //     // obscureText: _obscureTextSignupConfirm,
                            //     style: TextStyle(
                            //         fontFamily: "WorkSansSemiBold",
                            //         fontSize: 16.0,
                            //         color: Colors.black),
                            //     decoration: InputDecoration(
                            //       border: InputBorder.none,
                            //       icon: Icon(
                            //         Icons.lock,
                            //         color: Colors.black,
                            //       ),
                            //       hintText: "Jumlah",
                            //       hintStyle: TextStyle(
                            //           fontFamily: "WorkSansSemiBold", fontSize: 16.0),
                            //       suffixIcon: GestureDetector(
                            //         onTap: (){},
                            //         child: Icon(
                            //           Icons.panorama_fish_eye,
                            //           size: 15.0,
                            //           color: Colors.black,
                            //         ),
                            //       ),
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 340.0),
                      decoration: new BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: Theme.Colors.loginGradientStart,
                            offset: Offset(1.0, 6.0),
                            blurRadius: 20.0,
                          ),
                          BoxShadow(
                            color: Theme.Colors.loginGradientEnd,
                            offset: Offset(1.0, 6.0),
                            blurRadius: 20.0,
                          ),
                        ],
                        gradient: new LinearGradient(
                            colors: [
                              Theme.Colors.loginGradientEnd,
                              Theme.Colors.loginGradientStart
                            ],
                            begin: const FractionalOffset(0.2, 0.2),
                            end: const FractionalOffset(1.0, 1.0),
                            stops: [0.0, 1.0],
                            tileMode: TileMode.clamp),
                      ),
                      child: MaterialButton(
                          highlightColor: Colors.transparent,
                          splashColor: Theme.Colors.loginGradientEnd,
                          //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 12.0, horizontal: 42.0),
                            child: Text(
                              "KIRIM",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 25.0,
                                  fontFamily: "WorkSansBold"),
                            ),
                          ),
                          onPressed: () {
                            createData();
                          }
                              // showInSnackBar("SignUp button pressed")
                              ),
                    ),
                  ],
                ),
              ],
            ),
        ),
      ),
    )
      ),
      ),
    );
    
  }

 void createData() async {

         Firestore.instance
          .collection('listData')
          .add(person.toMap())
          .whenComplete(() {
        Navigator.pop(context);
      });
  }
  
}